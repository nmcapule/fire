# http://hiltmon.com/blog/2013/07/03/a-simple-c-plus-plus-project-structure/
# required folders: bin/ buikd/ docs/ lib/ src/

CC := clang++

DIR_SRC := src
DIR_BUILD := build

# output of executable
TARGET := bin/main

EXT_SRC := cc
EXT_HEADER := h

# uses /usr/bin/find because windows is a bitch
# finds all cpp files inside src folder
SOURCES := $(shell /usr/bin/find $(DIR_SRC) -type f -name *.$(EXT_SRC))

# replaces src with build and changes extension from cpp to o
# data is from sources, figure it out
OBJECTS := $(patsubst $(DIR_SRC)/%,$(DIR_BUILD)/%,$(SOURCES:.$(EXT_SRC)=.o))

# if you want warning all
CFLAGS := -g -std=c++11 -Wall

# additional libs here
LIB := -lpthread
INC := -I include

# apparently builds all objects first before building the main executable
$(TARGET): $(OBJECTS)
	@echo " Linking..."
	@mkdir -p $(@D)
	@echo " $(CC) $^ -o $(TARGET) $(LIB)"; $(CC) $^ -o $(TARGET) $(LIB)

# the snippet below always force recompiles main.cc
$(DIR_BUILD)/main.o: $(DIR_SRC)/main.cc
	@mkdir -p $(@D)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

# note: if a cc file is in a folder on src, must make corresponding
#       folder in build. might want to fix this later
$(DIR_BUILD)/%.o: $(DIR_SRC)/%.$(EXT_SRC)
	@mkdir -p $(@D)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

clean:
	@echo " Cleaning..."
	@echo " $(RM) -r $(DIR_BUILD) $(TARGET)"; $(RM) -r $(DIR_BUILD)/* $(TARGET)

.PHONY: clean $(DIR_BUILD)/main.o
