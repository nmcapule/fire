#include "socket.h"

namespace fire {

Socket::Socket() {
    init();
    configure();
}

Socket::Socket(int sd)
      : FileDescriptor(sd) {
    // assumed to be configured outside
}

Socket::~Socket() {
}

void Socket::configure() {
    int tmp = 1;
    if (setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR, &tmp, sizeof(int)) < 0)
        perror("Unable to configure socket");
}

void Socket::init() {
    fd_ = socket(AF_INET, SOCK_STREAM, 0);
    if (fd_ < 0) 
        perror("Unable to create socket");
}

int Socket::bind(int port) {
    addr_.sin_family = AF_INET;
    addr_.sin_addr.s_addr = INADDR_ANY;
    addr_.sin_port = htons(port);
    memset(&(addr_.sin_zero), '\0', 8);

    int ret = ::bind(fd_, (struct sockaddr*) &addr_, sizeof(addr_));
    if (ret < 0)
        perror("Unable to bind socket");

    return ret;
}

int Socket::listen(int backlog) {
    int ret = ::listen(fd_, backlog);
    if (ret < 0)
        perror("Failed to listen to socket");

    return ret;
}

Socket Socket::accept(struct sockaddr_in* cl_addr) {
    socklen_t cl_addr_len = sizeof(cl_addr);
    int ret =::accept(fd_, (struct sockaddr*) cl_addr, &cl_addr_len);
    if (ret < 0)
        perror("Failed to accept connection");

    return Socket(ret);
}

int Socket::close() {
    int ret = ::close(fd_);
    if (ret < 0)
        perror("Failed to close socket");

    return ret;
}

struct sockaddr_in* Socket::address() {
    return &addr_;
}

}
