#ifndef FIRE_SOCKET_H_
#define FIRE_SOCKET_H_

#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "../ipc/file_descriptor.h"

namespace fire {

class Socket : public FileDescriptor {
    struct sockaddr_in  addr_;

public:
    Socket();
    Socket(int sd_);
    virtual ~Socket();

    int bind(int port);
    int listen(int backlog=5);
    Socket accept(struct sockaddr_in* cl_addr);
    int close();

    struct sockaddr_in* address();
private:
    void configure();
    void init();
};

}

#endif
