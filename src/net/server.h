#ifndef FIRE_SERVER_H_
#define FIRE_SERVER_H_

#include <stdio.h>
#include <sys/select.h>

#include "socket.h"
#include "../thread/thread.h"

namespace fire {

class Server : public Thread {
    Socket sock_;

    int port_;
    int max_clients_;

    fd_set      select_fds_;
    int         fd_max_;

public:
    Server(int port, int max_clients);
    ~Server();

    Socket* sock();

protected:
    // return any from recv
    virtual int handle(Socket client_sock) =0;
    virtual void on_client_connect(Socket sock);
    virtual void on_client_close(Socket sock);
    virtual void on_client_error(Socket sock, int err);

private:
    virtual int handle_connect();
    virtual void* run();
    virtual void on_start();

    int init();
};

}

#endif
