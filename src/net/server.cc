#include "server.h"

namespace fire {

Server::Server(int port, int max_clients) 
      : Thread(), port_(port), max_clients_(max_clients) { }

Server::~Server() {
    
}

Socket* Server::sock() {
    return &sock_;
}

int Server::handle_connect() {
    struct sockaddr_in client_addr;
    Socket client_sock =  sock_.accept(&client_addr);
    int client_fd = client_sock.fd();

    if (client_fd > fd_max_)
        fd_max_ = client_fd;

    FD_SET(client_fd, &select_fds_);

    return client_fd;
}

void Server::on_client_connect(Socket sock) { }
void Server::on_client_close(Socket sock) { }
void Server::on_client_error(Socket sock, int err) { }

void* Server::run() {
    fd_set tmp_fds;
    FD_ZERO(&tmp_fds);

    while (is_running()) {
        tmp_fds = select_fds_;
        if (::select(fd_max_ + 1, &tmp_fds, NULL, NULL, NULL) < 0) {
            perror("Error in select");
            cancel();
        }

        for (int fd = 0; fd <= fd_max_; fd++) {
            if (FD_ISSET(fd, &tmp_fds)) {
                if (fd == sock_.fd()) {
                    Socket client_sock(handle_connect());

                    on_client_connect(client_sock);
                } else {
                    Socket client_sock(fd);

                    int res = handle(client_sock);
                    if (res <= 0) {
                        if (res == 0)   on_client_close(client_sock);
                        else            on_client_error(client_sock, res);

                        client_sock.close();
                        FD_CLR(client_sock.fd(), &select_fds_);
                    }
                }
            }
        }
    }
    return NULL;
}

void Server::on_start() {
    init();
}

int Server::init() {
    int ret = 0;
    if ((ret = sock_.bind(port_)) < 0)
        return ret;
    if ((ret = sock_.listen(5)) < 0)
        return ret;

    FD_ZERO(&select_fds_);
    FD_SET(sock_.fd(), &select_fds_);

    fd_max_ = sock_.fd();

    return 0;
}

}
