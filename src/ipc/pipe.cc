#include "pipe.h"

namespace fire {

Pipe::Pipe() {
    int fdwr_[2];
    pipe(fdwr_);

    fd_read_ = FileDescriptor(fdwr_[0]);
    fd_write_ = FileDescriptor(fdwr_[1]);
}

Pipe::~Pipe() {
    close_read();
    close_write();
}

void Pipe::close_read() {
    close(fd_read_.fd());
}

void Pipe::close_write() {
    close(fd_write_.fd());
}

FileDescriptor Pipe::fd_read() {
    return fd_read_;
}

FileDescriptor Pipe::fd_write() {
    return fd_write_;
}

}
