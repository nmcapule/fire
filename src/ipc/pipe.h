#ifndef FIRE_PIPE_H_
#define FIRE_PIPE_H_

#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "file_descriptor.h"

namespace fire {

class Pipe {
    FileDescriptor fd_read_;
    FileDescriptor fd_write_;

public:
    Pipe();
    virtual ~Pipe();

    void close_read();
    void close_write();

    template <typename T>
    ssize_t put(T* data, int count=1);
    template <typename T>
    ssize_t get(T* data, int count=1);

    FileDescriptor fd_read();
    FileDescriptor fd_write();
};

template <typename T>
ssize_t Pipe::put(T* data, int count) {
    return fd_write_.put<T>(data, count);
}

template <typename T>
ssize_t Pipe::get(T* data, int count) {
    return fd_read_.put<T>(data, count);
}

}

#endif
