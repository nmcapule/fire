#include "file_descriptor.h"

namespace fire {

FileDescriptor::FileDescriptor() { }

FileDescriptor::FileDescriptor(int fd)
      : fd_(fd) { }

FileDescriptor::~FileDescriptor() { }

int FileDescriptor::fd() {
    return fd_;
}

}
