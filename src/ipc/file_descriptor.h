#ifndef FIRE_FILE_DESCRIPTOR_H_
#define FIRE_FILE_DESCRIPTOR_H_

#include <unistd.h>

namespace fire {

class FileDescriptor { 
public:
    FileDescriptor();
    FileDescriptor(int fd);
    virtual ~FileDescriptor();

    template <typename T>
    ssize_t put(T* data, int count=1);
    template <typename T>
    ssize_t get(T* data, int count=1);

    int fd();

protected:
    int fd_;
};

template <typename T>
ssize_t FileDescriptor::put(T* data, int count) {
    return write(fd_, data, sizeof(data) * count);
}

template <typename T>
ssize_t FileDescriptor::get(T* data, int count) {
    return read(fd_, data, sizeof(data) * count);
}

}

#endif 
