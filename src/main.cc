#include <iostream>

#include <unistd.h>

#include "thread/thread.h"
#include "ipc/pipe.h"
#include "net/socket.h"
#include "net/server.h"

using namespace std;
using namespace fire;

class MyServer : public Server {
public:
    MyServer(int port, int max_clients)
          : Server(port, max_clients) { }

protected:
    int handle(Socket client_sock) {
        char buffer[256];
        int sz = client_sock.get<char>(buffer, 256);
        buffer[sz] = '\0';

        cout << "size: " << sz << endl;
        if (sz > 0) {
            cout << buffer << endl;
        }

        return sz;
    }

    void on_client_connect(Socket client_sock) {
        cout << "Connection accepted" << endl;
    }

    void on_client_close(Socket client_sock) {
        cout << "Connection close" << endl;
    }

    void on_client_error(Socket client_sock, int error) {
        cout << "Connection error" << endl;
    }
};

int main(void) {
    MyServer s(1337, 5);
    s.start();
    s.join();

	return 0;
}
