#include "thread.h"

namespace fire {

Thread::Thread(const char* name)
      : name_(name) {
}

Thread::~Thread() {
    if (is_running_)
        cancel();
}

int Thread::start() {
    pthread_create(&thread_id_, NULL, _static_run_, this);

    return 0;
}

int Thread::cancel() {
    pthread_cancel(thread_id_);

    return 0;
}

void* Thread::join() {
    void* p_ret;
    pthread_join(thread_id_, &p_ret);

    return p_ret;
}

bool Thread::is_running() {
    return is_running_;
}

pthread_t Thread::id() {
    return thread_id_;
}

const char* Thread::name() {
    return name_;
}

void Thread::on_start() { }
void Thread::on_stop() { }

void* Thread::_static_run_(void* me) {
    Thread* thread_me = (Thread*) me;
    void *p_ret;

    thread_me->on_start();

    thread_me->is_running_ = true;
    p_ret = thread_me->run();
    thread_me->is_running_ = false;

    thread_me->on_stop();

    return p_ret;
}

}
