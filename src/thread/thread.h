#ifndef FIRE_THREAD_H_
#define FIRE_THREAD_H_

#include <pthread.h>

namespace fire {

class Thread {
    const char*           name_;

    pthread_t       thread_id_;
    pthread_attr_t  *thread_attr_;

    bool is_running_;

public:
    Thread(const char* name="unnamed");
    virtual ~Thread();

    int start();
    int cancel();
    void* join();

    bool is_running();

    pthread_t id();
    const char* name();

protected:
    virtual void* run() =0;
    virtual void on_start();
    virtual void on_stop();

private:
    static void* _static_run_(void* me);
};

}

#endif
